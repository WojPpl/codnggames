//Power of Thor - Episode 1
//https://www.codingame.com/ide/puzzle/power-of-thor-episode-1
//author of solution: Wojciech Podsciborski s16496

import java.util.*;
import java.io.*;
import java.math.*;


class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int lightX = in.nextInt();
        int lightY = in.nextInt();
        int initialTx = in.nextInt();
        int initialTy = in.nextInt();

        int pthorX = initialTx;
        int pthorY = initialTy;

        while (true) {
            int remainingTurns = in.nextInt();
            String directionX="";
            String directionY="";

            if( pthorY > lightY ){
                directionY = "N";
                --pthorY;
            }
            else if( pthorY < lightY ){
                directionY = "S";
                ++pthorY;
            }
            if( pthorX > lightX ){
                directionX = "W";
                --pthorX;
            }
            else if( pthorX < lightX ){
                directionX = "E";
                ++pthorX;
            }
             System.out.println(directionY + directionX);

        }
    }
}